![Thomas More University of Applied Sciences](img/tm.png)
# Vagrant starter project - Linux Webservices
The following repository contains the starter files that accompany the Vagrant Tutorial for Linux Webservices. The tutorial can be found on [here](https://it-factory-thomas-more.gitlab.io/linux-web-services/course/linux-ws-course/10.vagrant/).


